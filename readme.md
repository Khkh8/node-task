# Welcome

Express project with *"Hello world!"* example

```
app.get('/', (req, res) => {
  res.send('Hello World!')
})
```

Start with cmd: `npm run start`

![alt](https://expressjs.com/images/express-facebook-share.png)
![altt](https://cdn.discordapp.com/emojis/746175880321302620.gif?v=1)
